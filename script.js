let player1;
let comp;
let result;

function getPilihanComp() {
    const comp = Math.random();
    if(comp < 0.34) return "batu"
    if(comp >= 0.34 && comp < 0.67) return "gunting";
    return "kertas"; 
}

function getHasil(comp, player1) {
    if(player1 == comp) return "Draw";
    if(player1 == "batu") return comp == "gunting" ? "Win" : "Lose";
    if(player1 == "gunting") return comp == "batu" ? "Lose" : "Win";
    if(player1 == "kertas") return comp == "gunting" ? "Lose" : "Win";
}

const pilihBatu = document.querySelector(".batu-p1");
pilihBatu.addEventListener("click", function() {
    const pilihanComputer = getPilihanComp();
    const pilihanPlayer1 = pilihBatu.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer1);

    const gambarComputer = document.querySelector(".batu-comp");
    gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

    const info = document.querySelector(".info");
    info.innerHTML = hasil;
});

const pilihGunting = document.querySelector(".gunting-p1");
pilihGunting.addEventListener("click", function() {
    const pilihanComputer = getPilihanComp();
    const pilihanPlayer1 = pilihGunting.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer1);

    const gambarComputer = document.querySelector(".gunting-comp");
    gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

    const info = document.querySelector(".info");
    info.innerHTML = hasil;
});

const pilihKertas = document.querySelector(".kertas-p1");
pilihKertas.addEventListener("click", function() {
    const pilihanComputer = getPilihanComp();
    const pilihanPlayer1 = pilihKertas.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer1);

    const gambarComputer = document.querySelector(".kertas-comp");
    gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

    const info = document.querySelector(".info");
    info.innerHTML = hasil;
});

function replay() {
    location.reload();
}